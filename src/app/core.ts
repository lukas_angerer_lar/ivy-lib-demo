import { InjectionToken } from '@angular/core';

export const RegistryToken = new InjectionToken<any>('RegistryToken');
