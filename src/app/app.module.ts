import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home.component';
import { AppComponent } from './app.component';
import { FooComponent } from './foo.component';
import { DynamicComponent } from './dynamic.component';
import { RegistryToken } from './core';
import { CommonModule } from '@angular/common';
import { DerivedFirstComponent } from './derived-first.component';
import { WrapperComponent } from './wrapper.component';


// import { registry } from '../../projects/shared/src/registry';

const registry = [];

// import { InfraModule, SharedModule } from '@my/shared';
// const registry = [
//   {
//       type: InfraModule,
//       module: () => import('@my/shared').then(m => { console.log('loaded', m); return m.InfraModule; }),
//   },
//   {
//       type: SharedModule,
//       module: () => import('@my/shared').then(m => m.SharedModule),
//   },
// ];

@NgModule({
  declarations: [
    AppComponent,
    WrapperComponent,
    FooComponent,
    HomeComponent,
    DynamicComponent,
    DerivedFirstComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
  ],
  providers: [
    {
      provide: RegistryToken,
      useValue: registry,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
