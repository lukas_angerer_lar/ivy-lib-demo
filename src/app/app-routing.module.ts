import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { DerivedFirstComponent } from './derived-first.component';
import { WrapperComponent } from './wrapper.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'foo', component: WrapperComponent },
  { path: 'lazy', loadChildren: () => import('@my/shared').then(m => {
    console.log('lazy route children loaded', m, m.SharedModule);
    return m.SharedModule;
  }) },
  { path: 'derived', component: DerivedFirstComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
