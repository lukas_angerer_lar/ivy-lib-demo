import {
    Component,
    Input,
    OnInit,
    ElementRef,
    ViewChild,
} from '@angular/core';
import { BaseComponent } from '@my/shared';

@Component({
    selector: 'app-derived-first',
    template: `<div>Derived <span #handle>component</span> content (first)</div>
    <div>Input text: {{ text }}</div>
    <div>Child element: {{ child.nativeElement.nodeName }}</div>`,
})
export class DerivedFirstComponent extends BaseComponent {

    constructor() {
        super();
    }

}
