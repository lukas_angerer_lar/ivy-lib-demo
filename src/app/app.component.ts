import { Component, SkipSelf, Injector, Inject, Optional } from '@angular/core';
import { SwitchValueProvider, SwitchValue, NameToken } from './fancy-providers';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [
  ]
})
export class AppComponent {
  title = 'ivy-lib';

  public constructor() {
  }
}
