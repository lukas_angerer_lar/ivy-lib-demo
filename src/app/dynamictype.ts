import {Type} from '@angular/core';

function foo(): Type<any> {
    const bar = function () {
        this.name = 'foo the bar';
    };
    return bar as unknown as Type<any>;
}

export const MyType = foo();
