import { Component, SkipSelf, Injector, Optional, Inject, InjectFlags } from '@angular/core';
import { SwitchValueProvider, SwitchValue, NameToken } from './fancy-providers';
// import Bloodhound from 'typeahead.js/dist/typeahead.bundle';
// import * as typeahead from 'typeahead.js/dist/typeahead.bundle';

@Component({
  selector: 'app-foo',
  template: `<p>FooBar</p>`,
  providers: [
    SwitchValueProvider.provide('app-foo'),
  ]
})
export class FooComponent {
  public constructor(v: SwitchValue, @SkipSelf() parentInjector: Injector, injector: Injector, @SkipSelf() @Optional() @Inject(NameToken) name: string) {
    // console.log('FooComponent', typeahead, typeahead.Bloodhound);
    console.log('FooComponent', v);
    console.log('Name token', injector.get(NameToken, null, InjectFlags.SkipSelf), name);
    console.log('--- injectors equal', parentInjector === injector, parentInjector, injector);

  }
}
