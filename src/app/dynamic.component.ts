import {
    Component,
    ViewContainerRef,
    Type,
    AfterViewInit,
    Inject,
    Input,
    Output,
    Compiler,
    NgModuleFactory,
    Injector,
    EventEmitter
} from '@angular/core';
import { RegistryToken } from './core';

@Component({
  selector: 'dynamic',
  template: ``
})
export class DynamicComponent implements AfterViewInit {
    @Input()
    public moduleType: Type<any>;

    @Output()
    public configureComponent: EventEmitter<any> = new EventEmitter();

    public constructor(
        private viewContainerRef: ViewContainerRef,
        private injector: Injector,
        private compiler: Compiler,
        @Inject(RegistryToken) private registry: any[]
    ) {
        console.log('dynamic component', this.registry);
    }

    public ngAfterViewInit(): void {
        const entry = this.registry.find(x => x.type.name === this.moduleType.name);
        if (entry) {
            entry.module().then(type => {
                console.log('module or factory', type, type instanceof NgModuleFactory);
                if (type instanceof NgModuleFactory) {
                    // For ViewEngine
                    return type;
                } else {
                    // For Ivy
                    // This doesn't actually _compile_ anything if the application was built in AOT mode
                    return this.compiler.compileModuleAsync(type);
                }
            }).then((moduleFactory: NgModuleFactory<any>) => {
                const bootComponent = (moduleFactory.moduleType as unknown as { bootComponent: Type<any> }).bootComponent;
                const moduleRef = moduleFactory.create(this.injector);

                const componentFactory = moduleRef.componentFactoryResolver.resolveComponentFactory(bootComponent);

                this.viewContainerRef.clear();
                const component = this.viewContainerRef.createComponent(componentFactory).instance;

                this.configureComponent.emit(component);
            });
        }
    }
}
