import { Component, SkipSelf, Injector, Optional, Inject, InjectFlags } from '@angular/core';
import { SwitchValueProvider, SwitchValue, NameToken } from './fancy-providers';

@Component({
  selector: 'app-wrapper',
  template: `<app-foo></app-foo>`,
  providers: [
    SwitchValueProvider.provide('app-wrapper'),
  ]
})
export class WrapperComponent {
  public constructor(
    v: SwitchValue,
    //@SkipSelf() parentInjector: Injector,
    injector: Injector,
    @SkipSelf() @Optional() @Inject(NameToken) name: string
  ) {
    // console.log('FooComponent', typeahead, typeahead.Bloodhound);
    console.log('WrapperComponent', v);
    console.log('Name token', injector.get(NameToken, null, InjectFlags.SkipSelf), name);
    //console.log('--- injectors equal', injector === injector, parentInjector, injector);

  }
}
