import { Component, Type, Inject } from '@angular/core';
import { InfraModule } from '@my/shared';
import { InfraComponent } from '../../projects/shared/src/public-api';
import { MyType } from './dynamictype';

export class Thingy {

}

@Component({
  selector: 'app-home',
  template: `<p>HOME</p>
<button type="button" (click)="loadStuff()">Load</button>
<dynamic *ngIf="show" [moduleType]="childType" (configureComponent)="onConfigure($event)">
`,
  providers: [
    {
      provide: MyType,
      useFactory: () => new MyType(),
    },
    Thingy
  ]
})
export class HomeComponent {
  public childType: Type<any> = InfraModule;
  public show: boolean = false;

  public constructor(
    @Inject(MyType) foobar: any,
    thing: Thingy
  ) {
    console.log(
      'HomeComponent metadata',
      (Reflect as any).getOwnMetadata,
      (Reflect as any).getOwnMetadata('custom:annotation',
      HomeComponent)
    );

    console.log('HomeComponent MyType', foobar, this);
  }

  public loadStuff(): void {
    this.show = true;
  }

  public onConfigure(component: InfraComponent): void {
    component.data = { test: 42 };
  }
}
