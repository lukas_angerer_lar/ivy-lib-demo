import { Injector, SkipSelf, Injectable, Provider, InjectionToken, Inject, InjectFlags } from '@angular/core';

export type BasicSwitchValue = string | number | boolean;

export const NameToken = new InjectionToken<any>('NameToken');
export const SwitchValueParentToken = new InjectionToken<any>('SwitchValueParentToken');

let switchId = 0;
let parentInjectorId = 0;

export class SwitchValue {
    public readonly id: number;

    constructor(private parent?: any) {
        this.id = switchId++;
        console.log(`created SwitchValue ${this.id} value with parent ${this.parent ? this.parent.id : null}`);
    }
}

@Injectable()
export class ParentInjectorResolver {
    constructor(public injector: Injector, @Inject(NameToken) public readonly name: string)  {
        //const v = this.injector.get(SwitchValue);
        console.log(`created ParentInjectorResolver ${name} and switch - ${injector.get(NameToken, null, InjectFlags.SkipSelf)}`);
    }
}

export class SwitchValueProvider {
public static provide(name: string): Provider {
    return [
        // To automate destruction of the FormCollection, we also have to provide this destruction sentry.
        // See DestructionSentry for more details.
        {
            provide: NameToken,
            useValue: name,
        },
        {
            provide: SwitchValueParentToken,
            useValue: { parent: name },
        },
        // {
        //     privide: SwitchValueParentToken,
        //     //useFactory: () => ({ parent: null }),
        //     //useFactory: () => 42,
        //     useValue: name,
        // },
        ParentInjectorResolver,
        {
            provide: SwitchValue,
            deps: [ParentInjectorResolver, SwitchValueParentToken],
            useFactory: (parentInjectorResolver: ParentInjectorResolver, dummy: { parent: SwitchValue }): SwitchValue => {
                let x = parentInjectorResolver.injector.get(ParentInjectorResolver);
                console.log(`factory with ppir ${x ? x.name : null}`);
                let t = parentInjectorResolver.injector.get(NameToken);
                console.log(`factory with name token ${t}`);

                const p = parentInjectorResolver.injector.get(SwitchValueParentToken);

                console.log('factory with value parent', dummy, p);
                const val = new SwitchValue(/*parentInjectorResolver.injector.get(SwitchValue)*/null)

                return val;
            }
        },
    ];
}}
