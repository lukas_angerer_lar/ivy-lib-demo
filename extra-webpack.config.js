const webpack = require('webpack');
const pkg = require('./package.json');

module.exports = (config, options) => {
  config.plugins.push(
    new webpack.DefinePlugin({
      'APP_VERSION': JSON.stringify(pkg.version),
    }),
  );

  console.log("-----------");
  /*console.log(JSON.stringify(config, (key, value) => {
    if (key === 'plugins') {
        if (value instanceof Array) {
            return value.map(x => x ? x.constructor.name : null);
        } else {
            return value;
        }
    }
    return value;
  }, '  '));*/
  console.log("-----------");

  return config;
};