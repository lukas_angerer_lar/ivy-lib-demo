import { InfraModule } from './lib/infra/infra.module';
import { SharedModule } from './lib/shared.module';

export const registry = [
    {
        type: InfraModule,
        module: () => import('@my/shared').then(m => { console.log('loaded', m); return m.InfraModule; }),
    },
    {
        type: SharedModule,
        module: () => import('@my/shared').then(m => m.SharedModule),
    },
];
