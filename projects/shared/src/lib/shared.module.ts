import { NgModule, Type } from '@angular/core';
import { SharedComponent } from './shared.component';
import { SharedRoutingModule } from './shared-routing.module';


@NgModule({
  declarations: [SharedComponent],
  imports: [
    SharedRoutingModule,
  ],
})
export class SharedModule {
  public static bootComponent: Type<SharedComponent> = SharedComponent;
}
