import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lib-infra',
  template: `
    <p>
      infra works! even with watching - sorta.
    </p>
    <pre><code>{{ data | json }}</code></pre>
  `,
  styles: []
})
export class InfraComponent implements OnInit {

  @Input()
  public data: object;

  constructor() { }

  ngOnInit() {
    console.log('initializing InfraComponent');
  }

}
