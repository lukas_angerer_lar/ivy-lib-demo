import { NgModule, Type } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfraComponent } from './infra.component';


@NgModule({
  declarations: [InfraComponent],
  imports: [
    CommonModule,
  ],
})
export class InfraModule {
  public static bootComponent: Type<InfraComponent> = InfraComponent;
}
