import {
    Component,
    Input,
    OnInit,
    ElementRef,
    ViewChild,
} from '@angular/core';

// @Component({
//     selector: 'lib-base',
//     template: `<div>Base <span #handle>component</span> content</div>`,
// })
export abstract class BaseComponent implements OnInit {

    @Input()
    public text: string;

    @ViewChild('handle', { static: true })
    public child: ElementRef;

    constructor() { }

    ngOnInit() {
        console.log('initializing BaseComponent');
    }

}