# IvyLib

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

`ng build shared` *MUST* be run before the server can be started since.

## Build

```sh
ng build shared
# .\node_modules\.bin\ivy-ngcc -s .\dist\
```

The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

* [Ng 8.0 + ivy + library Error : does not have 'ngModuleDef' property](https://github.com/angular/angular-cli/issues/14594)

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Notes
* `ng build shared` does not actually run `ivy-ngcc` on the library - the result is still a non-Ivy library
* `ng serve` seems to "add" some `__ivy_ngcc__` directory with Ivy files
* Running `.\node_modules\.bin\ivy-ngcc -s .\dist\` actually runs the Ivy compiler on the library (updating the source files)
* Understanding ng-packagr is essential for working with libraries productively
* ngcc (Angular compatibility compiler)
* ngtsc (Angular Ivy compiler) - uses TypeScript transforms and _output rewriting_
* components and/or directives that are _not declared_ anywhere seem to cause some issues... (not declaring the HomeComponent) leads to the DynamicHostDirective not being instantiated. Probably some issue with dependency detection.

## angular/cli Architect and Builders
* `ng run project:target` runs an architect target
* `ng build` is just a shorthand for a specific architect target
* custom builders are relatively easy to make
* repository of community custom builders https://github.com/just-jeb/angular-builders

## Buld, Watch
* watch mode seems glitchy
  * changes to NgModule dependencies seem to be particularly weird and require a "re-serve"
* `ng serve` does _not_ build in dist
* `ng serve` does _not_ build libraries in the project
* `ng build --watch @my/shared` combined with `ng serve` does reload, but it reloads _too ealy_

## Libraries
* Referencing libraries by their "external name" works, provided the configuration is correct
  * library package.json
  * angular.json => top-level library key
  * root tsconfig.json => path mappings

### Implications
Using Ivy for Viacar with _libraries_ will most likely have some or all of the following implications
* The libraries have the be *built* separately, they are no longer automatically _included_ with the watch task
  * https://github.com/angular/angular-cli/wiki/stories-create-library#why-do-i-need-to-build-the-library-everytime-i-make-changes
* Watching the main application won't do much since the changes are mostly going to be inside of the libraries
* We will have to spend some time on custom tooling around the new build process


## Webpack
* The "webpackChunkName" comment is _probably_ no longer needed with the NamedLazyChunksPlugin from Angular (https://github.com/Independer/angular-named-lazy-chunks-webpack-plugin#readme)
* 


## Things that are likely to break
* JIT/AOT template loading (with cshtml)
* chunk splitting


## Links
* https://blog.angularindepth.com/all-you-need-to-know-about-ivy-the-new-angular-engine-9cde471f42cf
  * dynamic loading of modules and components
  * higher order components
* [Angular Package Format (APF) v8.0](https://docs.google.com/document/d/1CZC2rcpxffTDfRDs6p1cfbmKNLA6x5O-NtkJglDaBVs/preview)
* [DESIGN DOC(Ivy): Compiler Architecture](https://github.com/angular/angular/blob/master/packages/compiler/design/architecture.md) (*PRIO 1*)

* https://blog.angularindepth.com/angular-cli-under-the-hood-builders-demystified-v2-e73ee0f2d811
* https://codeburst.io/customizing-angular-cli-6-build-an-alternative-to-ng-eject-a48304cd3b21

## Other Stuff
* [TypeScript - Using the Compiler API](https://github.com/Microsoft/TypeScript/wiki/Using-the-Compiler-API)
* [Creating a TypeScript Transformer](https://43081j.com/2018/08/creating-a-typescript-transform)
